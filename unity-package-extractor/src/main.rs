// Copyright (c) 2024 Zoe <zoe@zyoh.net>

use std::{
    error::Error,
    fs::File,
    path::{
        PathBuf,
        Path
    },
    io,
    env,
    process::exit
};

use clap::{
    Command,
    crate_name,
    crate_version,
    crate_authors,
    Arg,
    ValueHint,
    ArgAction
};
use flate2::read::GzDecoder;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

const DEFAULT_TEMP_DIRECTORY_NAME: &str = "tar";

fn main() {
    simple_logger::SimpleLogger::new().init().unwrap();
    log::set_max_level(log::LevelFilter::Info);

    let args = Command::new(crate_name!())
        .version(crate_version!())
        .about("Extracts Unity packages")
        .arg(
            Arg::new("authors")
                .long("authors")
                .help("Prints the authors of this program")
                .exclusive(true)
                .action(ArgAction::SetTrue)
        )
        .arg(
            Arg::new("input")
                .short('i')
                .long("input")
                .help("Archive to extract")
                .value_name("FILE")
                .value_hint(ValueHint::FilePath)
                .required_unless_present_any(["input_pos", "authors"])
        )
        .arg(
            Arg::new("input_pos")
                .value_hint(ValueHint::FilePath)
                .value_name("FILE")
                .required_unless_present_any(["input", "authors"])
                .index(1)
        )
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .help("Directory to extract to")
                .long_help("Directory to extract to. if `./foo` is specified, the archive will be extracted to `./foo/Assets/...`")
                .value_name("DIRECTORY")
                .value_hint(ValueHint::DirPath)
        )
        .arg(
            Arg::new("temp_directory")
                .long("tempdir")
                .help("Temporary directory for working files")
                .long_help(format!("Temporary directory for working files. Defaults to `<output>/{}`", DEFAULT_TEMP_DIRECTORY_NAME))
                .value_name("DIRECTORY")
                .value_hint(ValueHint::DirPath)
        )
        .arg(
            Arg::new("include_previews")
                .long("include-previews")
                .help("Include preview images for assets that support it")
                .long_help("Include preview images for assets that support it")
                .action(ArgAction::SetTrue)
        )
        .get_matches();

    // Print and exit
    if args.get_flag("authors") {
        println!("{}", crate_authors!());
        return;
    }

    let input_path = PathBuf::from(
        // ArgMatches guarantees one of these exists
        args.get_one::<String>("input").or(args.get_one::<String>("input_pos")).unwrap()
    );

    let output_path = match args.get_one::<String>("output") {
        Some(v) => PathBuf::from(v),
        None => PathBuf::from(input_path.with_extension("").to_string_lossy().trim_end())
    };

    let temp_directory = match args.get_one::<String>("temp_directory") {
        Some(v) => PathBuf::from(v),
        None => output_path.join(DEFAULT_TEMP_DIRECTORY_NAME)
    };

    // Create temp dir and ignore error (if exists).
    // Does not create parent directories.
    let _ = std::fs::create_dir(&temp_directory);

    log::debug!("Input path: {:?}", input_path);
    log::debug!("Output path: {:?}", output_path);
    log::debug!("Temp directory: {:?}", temp_directory);

    let args = Args {
        input_path,
        output_dir_path: output_path,
        temp_dir_path: temp_directory,
        include_previews: args.get_flag("include_previews")
    };

    if let Err(e) = args.make_outputs() {
        log::error!("failed to make output directories: {e}");
        exit(1);
    }

    match args.canonicalize() {
        Ok(_) => log::debug!("canonicalized all given paths"),
        Err(e) => {
            log::error!("failed to find one or more paths: {e}");
            exit(1);
        }
    }

    // Extract the archive to the temp directory
    if let Err(e) = extract_archive(&args.input_path, &args.temp_dir_path) {
        log::error!("failed to extract archive: {e}");
        exit(1);
    }

    let glob_pattern = format!("{}/*", &args.temp_dir_path.to_string_lossy());
    log::debug!("Glob pattern: {}", glob_pattern);

    for tar_directory in glob::glob(&glob_pattern).expect("Failed to glob").flatten() {
        log::trace!("{:?}", tar_directory);
        match process_tar_directory(&tar_directory, &args.output_dir_path, &args) {
            Ok(_) => log::debug!("successfully processed tar directory: {tar_directory:?}"),
            Err(e) => log::debug!("failed to process tar directory: {tar_directory:?}; {e}"),
        }
    }
}

fn process_tar_directory(tar_directory: &Path, output_directory: &Path, args: &Args) -> Result<()> {
    let (tar_pathname_path, tar_asset_path, tar_asset_meta_path) = match (
        tar_directory.join("pathname").canonicalize(),
        tar_directory.join("asset").canonicalize(),
        tar_directory.join("asset.meta").canonicalize()
    ) {
        (Ok(a), Ok(b), Ok(c)) => (a, b, c),
        _ => return Err("invalid tar folder structure".into())
    };
    let tar_preview_path = tar_directory.join("preview.png").canonicalize();

    // Only use first line of pathname file. I don't know what the other lines are for.
    let pathname = std::fs::read_to_string(tar_pathname_path)?;
    let pathname = pathname.split('\n').collect::<Vec<&str>>();
    let pathname = pathname.first().ok_or("invalid pathname file")?;

    let asset_path = output_directory.join(&pathname);
    let asset_meta_path = output_directory.join(format!("{}.meta", &pathname));

    std::fs::create_dir_all(asset_path.parent().ok_or("asset has no parent")?)?;

    std::fs::rename(tar_asset_path, asset_path)?;
    std::fs::rename(tar_asset_meta_path, asset_meta_path)?;

    if args.include_previews {
        if let Ok(tar_preview_path) = tar_preview_path {
            let preview_path = output_directory.join(format!("{}.preview.png", &pathname));
            std::fs::rename(tar_preview_path, preview_path)?;
        }
    }

    Ok(())
}

struct Args {
    input_path: PathBuf,
    output_dir_path: PathBuf,
    temp_dir_path: PathBuf,
    include_previews: bool
}

impl Args {
    fn canonicalize(&self) -> io::Result<()> {
        self.input_path.canonicalize()?;
        self.output_dir_path.canonicalize()?;
        self.temp_dir_path.canonicalize()?;

        Ok(())
    }

    fn make_outputs(&self) -> io::Result<()> {
        // Order matters here since temp dir is within output dir

        if let Err(e) = std::fs::create_dir(&self.output_dir_path) {
            if e.kind() != io::ErrorKind::AlreadyExists {
                return Err(e);
            }
        };

        if let Err(e) = std::fs::create_dir(&self.temp_dir_path) {
            if e.kind() != io::ErrorKind::AlreadyExists {
                return Err(e);
            }
        };

        Ok(())
    }
}


/// Extract a tar archive to a directory
fn extract_archive(archive_path: &Path, output_path: &Path) -> Result<()> {
    log::info!("Extracting archive `{:?}` to `{:?}`", archive_path, output_path);

    log::trace!("Opening archive file `{:?}`", archive_path);
    let archive_file = File::open(archive_path)?;

    log::trace!("Creating GzDecoder");
    let decoder = GzDecoder::new(archive_file);

    log::trace!("Creating tar::Archive");
    let mut archive = tar::Archive::new(decoder);

    log::trace!("Unpacking archive to `{:?}`", output_path);
    archive.unpack(output_path)?;

    log::info!("Successfully extracted archive `{:?}` to `{:?}`", archive_path, output_path);
    Ok(())
}
