# unity-package-extractor

# Usage

For basic usage, drag and drop a unity package file onto the executable to extract it into the package's directory.

For more options, run help `<executable> --help` in your console.

# License

Source code licensed under [MPL-2.0](./LICENSE) by `Zoe <zoe@zyoh.net>`.
